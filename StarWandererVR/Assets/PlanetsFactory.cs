﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets
{
    public class PlanetsFactory
    {
        const float semiAxisRatio = 6.0f;
        private planetsConfig _planetsConfig;
        private const string configFile = "config/planets";
        private static PlanetsFactory _instance = null;
        static public PlanetsFactory GetInstance()
        {
            if (_instance == null)
            {
                _instance = new PlanetsFactory();
                _instance.LoadConfig();
            }
            return _instance;
        }


        public List<Planet> CreateRandomPlanetsAroundStar(GameObject star)
        {
            List<Planet > planets = new List<Planet>();
            var positions = CreatePositionsAroundStar(star);
            foreach(var pos in positions)
            {
                var p = CreateRandom(star, pos.x, pos.y, pos.z);
                planets.Add(p);
            }
            return planets;
        }

        public List<Vector3> CreatePositionsAroundStar(GameObject star)
        {
            List<Vector3> planetsPos = new List<Vector3>();
            Star theStar = star.GetComponent<Star>();
            var diameterStar = theStar.GetMagnitude();
            int planetCount= UnityEngine.Random.Range(2, 7);
            var xValues = GetRandomUniqueValues(planetCount, 0, 20);
            for (int i = 0; i < xValues.Length; ++i)
            {
                var r = xValues[i] * (semiAxisRatio * diameterStar) / 20.0f + 3*diameterStar;
                var theta = UnityEngine.Random.Range(0, 360);
                float x = r * Mathf.Cos(theta);
                float z = r * Mathf.Sin(theta);
                float y = UnityEngine.Random.Range(-r * 0.005f, r * 0.005f);
                planetsPos.Add(new Vector3(x, y, z));
            }
            return planetsPos;
        }

        static public int[] GetRandomUniqueValues(int count, int min, int max)
        {
            HashSet<int> values = new HashSet<int>();
            int[] vals = new int[count];
            int value;
            for (int i = 0; i < count; i++)
            {
                do
                {
                    value = UnityEngine.Random.Range(min, max);
                }
                while ((values.Contains(value)) && (values.Count <= count));
                values.Add(value);
                vals[i] = value;
            }
            return vals;
        }


        public Planet CreateRandom(GameObject parent, float x, float y, float z)
        {
            GameObject obj = (GameObject)UnityEngine.Object.Instantiate(Resources.Load("Planet"), Vector3.zero , Quaternion.identity);
            if (obj)
            { 
                Planet objPlanet = obj.GetComponent<Planet>();
                objPlanet.PlanetConfig = GetRandomPlanetConfig();
                obj.transform.SetParent(parent.transform);
                obj.transform.localPosition = new Vector3(x, y, z);
                return objPlanet;
            }
            return null;
        }

        planet GetRandomPlanetConfig()
        {
            int rd = UnityEngine.Random.Range(0, _planetsConfig.planets.Length);
            return _planetsConfig.planets[rd];
        }

        private void LoadConfig()
        {
            TextAsset file = Resources.Load<TextAsset>(configFile);
            _planetsConfig = JsonUtility.FromJson<planetsConfig>(file.text);
            
        }
    }
    [Serializable]
    public class planetsConfig
    {
        public planet[] planets;
        public int version = 0;
    }
    [Serializable]
    public class planet
    {
        // json attributes from files
        public string name;
        public string description;
        public string texture;
        public int scale;
    }
}
