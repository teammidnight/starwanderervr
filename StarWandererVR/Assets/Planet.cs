﻿using UnityEngine;
using System.Collections;
using System;
using Assets;
using Assets.Scripts;

[Serializable]
public class Planet : MonoBehaviour {

    public GameObject Sphere;

    // json attributes from files
    public planet PlanetConfig;
    

    private Vector3 _initialLocalScale = Vector3.one;       
    
    // Use this for initialization
    void Start()
    {
        var worldConsts = WorldConsts.GetInstance();
        worldConsts.PropertyChanged += WorldConsts_PropertyChanged;  

        if (Sphere)
        {
            if (!string.IsNullOrEmpty(PlanetConfig.texture))
            {
                Sphere.GetComponent<Renderer>().material = Resources.Load("Material/" + PlanetConfig.texture) as Material;
            }
            _initialLocalScale = Sphere.transform.localScale;
            ComputeScale();
        }
        _rotOffset = gameObject.transform.eulerAngles.y;
    }

    void OnDestroy()
    {
        WorldConsts.GetInstance().PropertyChanged -= WorldConsts_PropertyChanged;
    }

    private void WorldConsts_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
        if (e.PropertyName == "PlanetScale")
        {
            ComputeScale();
        }
    }

    private void ComputeScale()
    {
        if (Sphere)
        {
            var scale = _initialLocalScale * PlanetConfig.scale * WorldConsts.GetInstance().PlanetScale;
            Sphere.transform.localScale = scale;
        }
    }
            

    private float _rotAngle = 0;
    private float _rotOffset = 0;
    private float _rotDuration = 10.0f;
    private float _time = 0;

    void FixedUpdate()
    {
        _rotAngle = Mathf.Lerp(0, 360, _time / _rotDuration);
        if (_time > _rotDuration)
        {
            _time = 0;
            _rotAngle = 0;
        }
        
        gameObject.transform.eulerAngles = new Vector3(0, _rotOffset + _rotAngle, 0);    
        _time += Time.fixedDeltaTime;
    }
}
