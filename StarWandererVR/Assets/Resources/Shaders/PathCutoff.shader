﻿Shader "Custom/PathCutoff" {
     Properties {
        _MainTex ("Base (RGB)", 2D) = "white" {}
        _MinColor ("Color in Minimal", Color) = (1, 1, 1, 1)
        _MaxColor ("Color in Maxmal", Color) = (0, 0, 0, 0)
        _MinDistance ("Min Distance", Float) = 0
        _MaxDistance ("Max Distance", Float) = 30
     }
     SubShader {
         Tags { "RenderType"="Opaque" }
         LOD 200
         
         CGPROGRAM
         #pragma surface surf Lambert
 
         sampler2D _MainTex;
 
         struct Input {
             float2 uv_MainTex;
             float3 worldPos;
         };
 
         float _MaxDistance;
         float _MinDistance;
         half4 _MinColor;
         half4 _MaxColor;
 
         void surf (Input IN, inout SurfaceOutput o) {
             half4 c = tex2D (_MainTex, IN.uv_MainTex);
             float dist = distance(_WorldSpaceCameraPos, IN.worldPos);
             half weight = saturate( (dist - _MinDistance) / (_MaxDistance - _MinDistance) );
             half4 distanceColor = lerp(_MinColor, _MaxColor, weight);
 
             o.Emission = c.rgb * distanceColor.rgb;
             o.Alpha = c.a;
         }
         ENDCG
     } 
     FallBack "Diffuse"
}