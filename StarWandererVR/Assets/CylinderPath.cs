﻿using UnityEngine;
using System.Collections;

public class CylinderPath : MonoBehaviour {

    public GameObject Cylinder;
    private Color _selectColor = new Color (0,200,50);
	// Use this for initialization
	void Start () {
	
	}
	

	// Update is called once per frame
	void FixedUpdate ()
    {
        var camera = Camera.main;
        GameObject gm = camera.gameObject; 
        float dist = (gm.transform.position - gameObject.transform.position).magnitude;
        if (dist < 4)
        {
            Cylinder.gameObject.GetComponent<MeshRenderer>().material.color = _selectColor;
        }   
        Cylinder.SetActive(dist < 50);           
	}
}
