﻿using UnityEngine;
using System.Collections;
using Assets;
using System;
using System.Collections.Generic;
using Assets.Scripts;

public class Star : MonoBehaviour {

    public GameObject Sphere;
    // json attributes from files
    public star StarConfig;
  
    public Color Color;
    
    private Vector3 _initialLocalScale = Vector3.one;

    public List<Planet> Planets = new List<Planet>();
    void Awake()
    {
       
    }

	// Use this for initialization
	void Start ()
    {
        var worldConsts = WorldConsts.GetInstance();
        worldConsts.PropertyChanged += WorldConsts_PropertyChanged;

        if (Sphere)
        {
            if (!string.IsNullOrEmpty(StarConfig.texture))
            {
                Sphere.GetComponent<Renderer> ().material = Resources.Load("Material/" + StarConfig.texture) as Material;
            }
            _initialLocalScale = Sphere.transform.localScale;
            ComputeScale();
        }
        var light = this.GetComponent<Light>();
        light.range += 4*StarConfig.scale;
        Planets = PlanetsFactory.GetInstance().CreateRandomPlanetsAroundStar(this.gameObject);
        _rotOffset = gameObject.transform.eulerAngles.y;
    }

    void OnDestroy()
    {
        WorldConsts.GetInstance().PropertyChanged -= WorldConsts_PropertyChanged;
    }

    private void WorldConsts_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
        if (e.PropertyName == "StarScale")
        {
            ComputeScale();
        }
    }

    private void ComputeScale()
    {
        if (Sphere)
        {
            var scale = _initialLocalScale * StarConfig.scale * WorldConsts.GetInstance().StarScale;
            Sphere.transform.localScale = scale;
        }
    }
    private float _rotAngle = 0;
    private float _rotOffset = 0;
    private float _rotDuration = 120.0f;
    private float _time = 0;

    void FixedUpdate()
    {
        _rotAngle = Mathf.Lerp(0, 360, _time / _rotDuration);
        if (_time > _rotDuration)
        {
            _time = 0;
            _rotAngle = 0;
        }

        gameObject.transform.eulerAngles = new Vector3(0, _rotOffset + _rotAngle, 0);
        _time += Time.fixedDeltaTime;
    }


    public float GetMagnitude()
    {
        if (Sphere != null)
        {
            return Sphere.GetComponent<MeshRenderer>().bounds.extents.x;
        }
        return 1.0f;
    }
}
