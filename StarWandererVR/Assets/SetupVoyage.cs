﻿using UnityEngine;
using System.Collections;
using Assets;
using System.Collections.Generic;

public class SetupVoyage : MonoBehaviour {

    public List<Star> Stars;

	// Use this for initialization
	void Start () 
    {
        
        var factory = StarsFactory.GetInstance();
        var positions = factory.CreateStarsPositions(35);
        var unorderedStars = factory.CreateStars(positions);
        Stars = factory.OrderedStarsForPath(unorderedStars, Vector3.zero);
    }

    // Update is called once per frame
    void Update () {
	
	}
}
