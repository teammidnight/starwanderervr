﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    class WorldConsts : INotifyPropertyChanged
    {
        private float _worldSize = 200.0f; // World is going from -WorldSize to +WorldSize in 3 Axis
        private float _starScale = 1.0f;
        private float _planetScale = 0.35f;
       
        public event PropertyChangedEventHandler PropertyChanged;

        private static WorldConsts _instance = null;
        static public WorldConsts GetInstance()
        {
            if (_instance == null)
            {
                _instance = new WorldConsts();
            }
            return _instance;
        }

        public float WorldSize
        {
            get { return _worldSize; }
            set
            {
                _worldSize = value;
                OnPropertyChanged("WorldSize");
            }
        }

        public float StarScale
        {
            get { return _starScale; }
            set
            {
                _starScale = value;
                OnPropertyChanged("StarScale");
            }
        }
        public float PlanetScale
        {
            get { return _planetScale; }
            set
            {
                _planetScale = value;
                OnPropertyChanged("PlanetScale");
            }
        }

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
