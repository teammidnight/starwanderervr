﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Wanderer : MonoBehaviour {
    public float time;
    LTSpline cr;
    LTDescr lt;
    private bool _init = false;
    private List<Vector3> _waypoints = new List<Vector3> { Vector3.zero };

    public int PathVertexCount = 30000;
    public float LineWidth = 1.0f;
    public float LineUpwardsOffset = 3.0f;

    // Use this for initialization
    void Start () { }

    // Update is called once per frame
    void Update () {
        if (!_init) {
            var camera = Camera.main.transform.parent;
            var sv = this.GetComponent<SetupVoyage> ();
            
            sv.Stars.ForEach (s => {
                _waypoints.Add(s.Planets[0].transform.position);
                _waypoints.Add(s.Planets[1].transform.position);
            });
            _waypoints.Add (Vector3.zero);

            cr = new LTSpline (_waypoints.ToArray());
            lt = LeanTween.moveSpline(camera.gameObject, cr.pts, time).setOrientToPath(false).setRepeat(-1).setEase(LeanTweenType.linear);
            GeneratePathSplineView();
            _init = true;
        }
    }

    void GeneratePathSplineView()
    {
        var renderer = GameObject.Find("PathRenderer");
        var meshFilter = renderer.GetComponent<MeshFilter>();
        
        var count = PathVertexCount;
        var fcount = (float)count;
        var halfWidth = LineWidth / 2;
        var colinearEpsilon = 0.05f;
        var offset = LineUpwardsOffset;

        var vertices = new Vector3[2*count];
        var tris = new int[(count-1) * 12];

        var vec0 = cr.point(0.0f);
        var vec1 = cr.point(1.0f / fcount);
        var vec2 = cr.point(2.0f / fcount);

        var previousForward = (vec1 - vec0).normalized;

        // P0 calculation
        var forward = (vec2 - vec1).normalized;
        var normal = Vector3.Cross(forward,previousForward).normalized;
        while ((1.0f - Vector3.Dot (forward, normal)) < colinearEpsilon) {
            normal = new Vector3 (Random.Range(0.0f,1.0f), Random.Range(0.0f,1.0f), Random.Range(0.0f,1.0f)).normalized;
        }
        var previousNormal = normal;

        vertices[0] = vec0 + halfWidth * normal;
        vertices[1] = vec0 - halfWidth * normal;

        tris[0] = 1;
        tris[1] = 0;
        tris[2] = 3;

        tris[3] = 1;
        tris[4] = 3;
        tris[5] = 0;

        tris[6] = 0;
        tris[7] = 2;
        tris[8] = 3;

        tris[9] = 0;
        tris[10] = 3;
        tris[11] = 2;

        for (int i = 2; i < count - 1; ++i)
        {
            vec2 = cr.point(i / fcount);

            forward = (vec2 - vec1).normalized;

            if (1.0f - Vector3.Dot (forward, previousForward) < colinearEpsilon) {
                normal = previousNormal;
            } else {
                normal = Vector3.Cross (forward, previousForward).normalized;
            }

            var up = Vector3.Cross (normal, forward).normalized;

            var baseIdx = 2 * (i - 1);

            vertices[baseIdx] = vec1 + halfWidth * normal + up*offset;
            vertices[baseIdx+1] = vec1 - halfWidth * normal + up*offset;

            tris[12 * i] = baseIdx + 1;
            tris[12 * i + 1] = baseIdx;
            tris[12 * i + 2] = baseIdx + 3;

            tris[12 * i + 3] = baseIdx + 1;
            tris[12 * i + 4] = baseIdx + 3;
            tris[12 * i + 5] = baseIdx;

            tris[12 * i + 6] = baseIdx;
            tris[12 * i + 7] = baseIdx + 2;
            tris[12 * i + 8] = baseIdx + 3;

            tris[12 * i + 9] = baseIdx;
            tris[12 * i + 10] = baseIdx + 3;
            tris[12 * i + 11] = baseIdx + 2;

            vec0 = vec1;
            vec1 = vec2;

            previousForward = forward;
            previousNormal = normal;
        }

        vertices[count-2] = vec2 + halfWidth * normal;
        vertices[count-1] = vec2 - halfWidth * normal;

        meshFilter.mesh = new Mesh { vertices = vertices, triangles = tris };
    }

    public void OnPauseClicked() {
        lt.pause ();
    }

    public void OnResumeClicked() {
        lt.resume ();
    }

    public void OnBrightClicked() {
    }

    public void OnNormalClicked() {
    }

    public void OnDimClicked() {
    }
}
