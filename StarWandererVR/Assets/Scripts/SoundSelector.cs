﻿using UnityEngine;
using System.Collections;

public class SoundSelector : MonoBehaviour {

	// Use this for initialization
	void Start () {
        var source = this.GetComponent<AudioSource> ();
        source.clip = Resources.Load<AudioClip> (MenuResult.AudioAsset) as AudioClip;
        source.Play ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
