﻿using UnityEngine;
using System.Collections;

public static class MenuResult {
    public static string AudioAsset = "Audio/In_Your_Own_Time_192kbit_AAC_";
}

public class ButtonsManager : MonoBehaviour {
    
	// Use this for initialization
	void Start () {
         
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnChillClick() {
        MenuResult.AudioAsset = "Audio/In_Your_Own_Time_192kbit_AAC_";
    }

    public void OnIndieClick() {
        MenuResult.AudioAsset = "Audio/Boards_of_Canada_-_Dayvan_Cowboy_128kbit_AAC_";
    }

    public void OnClassicClick() {
        MenuResult.AudioAsset = "Audio/F_Chopin_NOTTURNO_Op_9_n_2_Pianista_Olga_Bordas_12";
    }

    public void OnUnwindClick() {
        MenuResult.AudioAsset = "Audio/Chapelier_Fou_-_Protest_128kbit_AAC_";
    }

    public void OnStartClick() {
        UnityEngine.SceneManagement.SceneManager.LoadScene("voyage");
    }
}
