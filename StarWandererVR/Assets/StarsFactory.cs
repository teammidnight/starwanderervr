﻿using Assets.Scripts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets
{
    public class StarsFactory
    {        
        private starsConfig _starsConfig;
        private const string configFile = "config/stars";
        private static StarsFactory _instance = null;
        static public StarsFactory GetInstance()
        {
            if (_instance == null)
            {
                _instance = new StarsFactory();
                _instance.LoadConfig();
            }
            return _instance;
        }



        public List<Star> OrderedStarsForPath(List<Star> stars, Vector3 userStartPosition, int randomizationFactor = 0)
        {
            var remainingStars = new List<Star>(stars);
            var orderedStars = new List<Star>();
            var currentPos = userStartPosition;
            while (remainingStars.Count > 0)
            {
                var starsDistances = GetStarsDistancesFrom(remainingStars, currentPos);
                starsDistances.Sort((x, y) => x._distance.CompareTo(y._distance));
                var index = UnityEngine.Random.Range(0, Math.Min(randomizationFactor, starsDistances.Count - 1));
                Star s = starsDistances[index]._star;
                orderedStars.Add(s);
                remainingStars.Remove(s);
                currentPos = s.transform.position;
            }
            return orderedStars;
        }

        public class StarDistance
        {
            public Star _star;
            public float _distance;
        }
        public List<StarDistance> GetStarsDistancesFrom(List<Star> stars, Vector3 from)
        {
            List<StarDistance> starsDistances = new List<StarDistance>(); 
            stars.ForEach(s => {
                float dist = (s.transform.position - from).magnitude;
                starsDistances.Add( new StarDistance() { _star = s, _distance = dist } );
                });
            return starsDistances;
        }

        public List<Star> CreateStars(List<Vector3>positions)
        {
            List<Star> stars = new List<Star>();
            foreach(var pos in positions )
            {
                var star = CreateRandom(pos.x, pos.y, pos.z);
                stars.Add(star);
            }
            return stars;
        }

       public List<Vector3> CreateStarsPositions(int count)
        {
            List<Vector3> starspos = new List<Vector3>();
            while (starspos.Count < count)
            {
                var vec = CreateRandomPosition();
                if (IsGoodDistance(vec, starspos, 70))
                {
                    starspos.Add(vec);
                }
            }
            return starspos;
        }

        private bool IsGoodDistance(Vector3 newPos, List<Vector3> others, float minDist)
        {
            foreach(var pos in others)
            {
                var d = newPos - pos;
                if (Mathf.Abs(d.magnitude) < minDist)
                {
                    return false;
                }
            }
            return true;
        }


        public Vector3 CreateRandomPosition()
        {
            var worldSize = WorldConsts.GetInstance().WorldSize;
            float x = UnityEngine.Random.Range(-worldSize, worldSize);
            float y = UnityEngine.Random.Range(-worldSize, worldSize);
            float z = UnityEngine.Random.Range(-worldSize, worldSize);
            return new Vector3(x, y, z);
        }


        public Star CreateRandom()
        {
            var worldSize = WorldConsts.GetInstance().WorldSize;
            float x = UnityEngine.Random.Range(-worldSize, worldSize);
            float y = UnityEngine.Random.Range(-worldSize, worldSize);
            float z = UnityEngine.Random.Range(-worldSize, worldSize);
            return CreateRandom(x, y, z);
        }

        public Star CreateRandom(float x, float y, float z)
        {
            GameObject obj = (GameObject)UnityEngine.Object.Instantiate(Resources.Load("Star"), new Vector3(x, y, z), Quaternion.identity);
            if (obj)
            { 
                Star objStar = obj.GetComponent<Star>();
                objStar.StarConfig = GetRandomStarConfig();                
                return objStar;
            }
            return null;
        }

        star GetRandomStarConfig()
        {
            int rd = UnityEngine.Random.Range(0, _starsConfig.stars.Length);
            return _starsConfig.stars[rd];
        }

        private void LoadConfig()
        {
            TextAsset file = Resources.Load<TextAsset>(configFile);
            _starsConfig = JsonUtility.FromJson<starsConfig>(file.text);
            
        }
    }
    [Serializable]
    public class starsConfig
    {
        public star[] stars;
        public int version = 0;
    }
    [Serializable]
    public class star
    {
        // json attributes from files
        public string name;
        public string description;
        public string texture;
        public int scale;
    }
}
